
def boi_tinh_duyen(ten_nam, ten_nu):
    ten_nam = ten_nam.lower()
    ten_nu = ten_nu.lower()
    dem = 0
    for i in range(ord('a'), ord('z') + 1):
        # print(chr(i))
        if chr(i) in ten_nam and chr(i) in ten_nu:
            dem = dem + 1

    if dem == 0:
        ket_qua = 'xa'
    elif dem < 4:
        ket_qua = 'ban'
    else:
        ket_qua = 'hop nhau'

    return (ket_qua)


print('ten nam la: ')
ten_nam = input()
# ten_nam = 'Nguyen Gia Vy'
print('-----')

print('ten nu la: ')
ten_nu = input()
# ten_nu = 'Phan Thi To Uyen'

print(boi_tinh_duyen(ten_nam, ten_nu))
