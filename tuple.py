tup = (1, 2, 3, 4, 5, 6, 7, 9, 8)

# Sử dụng method count()
print(tup.count(5))  # Output: 1

# Sử dụng method index()
print(tup.index(3))  # Output: 2

# Sử dụng method len()
print(len(tup))  # Output: 9
