class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def say_hello(self):
        print("Hello, my name is", self.name,
              "and I am", self.age, "years old.")


man_1 = Person("John", 25)
man_1.say_hello()  # kết quả: Hello, my name is John and I am 25 years old.

man_2 = Person("Kong", 20)
man_2.say_hello()
