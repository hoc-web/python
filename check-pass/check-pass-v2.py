import re
regex_pass = re.compile(
    r"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")


def check_pass(password):
    check = regex_pass.fullmatch(password)
    if check:
        print('pass ok')
    else:
        print('pass not ok')


password = 'ngvNG#$%ads'
check_pass(password)
