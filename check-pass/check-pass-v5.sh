#!/bin/bash

function pwned_api_check() {
    local password="$1"
    local sha1password=$(echo -n "$password" | openssl sha1 | awk '{print toupper($2)}')
    local first5_char="${sha1password:0:5}"
    local tail="${sha1password:5}"
    local url="https://api.pwnedpasswords.com/range/$first5_char"
    local response=$(curl -s "$url")
    local count=$(echo "$response" | grep -i "$tail" | awk -F ':' '{print $2}')

    if [ -z "$count" ]; then
        echo "$password was not found"
    else
        echo "$password was found $count times"
    fi
}

while true; do
    read -p 'type "break" to exit
passwords (separated by commas): ' passwords

    if [ "$passwords" == "break" ]; then
        break
    fi

    IFS=',' read -ra password_array <<< "$passwords"
    for password in "${password_array[@]}"; do
        pwned_api_check "$password"
    done
done
