import re
regex_pass = re.compile(
    r"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")

password = 'ngvNG7#$%ads'

check = regex_pass.fullmatch(password)

print(check)
