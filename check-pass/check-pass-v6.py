import requests
import hashlib

def pwned_api_check(password):
    sha1password = hashlib.sha1(password.encode('utf-8')).hexdigest().upper()
    first5_char, tail = sha1password[:5], sha1password[5:]
    url = 'https://api.pwnedpasswords.com/range/' + first5_char
    with requests.get(url) as res:
        if res.status_code != 200:
            raise RuntimeError(f'Error fetching: {res.status_code} check the API')
        # print(f'res: {res.text.splitlines()} \n')
        # print(f'pass is: \n {res.text}')
        # print(f'type of res.text: {type(res.text)}')
        hashes = (line.split(':') for line in res.text.splitlines())
        count = next((int(count) for h, count in hashes if h == tail), 0)
    return count

if __name__ == '__main__':
    while True:
        passwords = input('type "break" to exit\npasswords (separated by commas): ')
        if passwords == 'break':
            break
        for password in passwords.split(','):
            count = pwned_api_check(password)
            if count:
                print(f'{password} was found {count} times')
            else:
                print(f'{password} was not found')
