from translate import Translator
trans = Translator(to_lang="vi")

try:
    with open('./test.txt', mode='r') as my_file:
        text = my_file.read()
        text_2 = trans.translate(text)
        with open('./test_2.txt', mode='w') as my_file_2:
            my_file_2.write(text_2)
            print(text_2)

except FileNotFoundError as e:
    print('File Not Found')
